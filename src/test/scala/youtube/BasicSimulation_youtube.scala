package youtube

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation_youtube extends Simulation {

  val httpProtocol = http
    .baseUrl("https://www.youtube.com/") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Ubuntu  rv:18.04) Gecko/20100101 Firefox Quantum/65.0.1")

  val scn = scenario("As a YouTube Guest find favorite Singer") // A scenario is a chain of requests and pauses
    .exec(http("request_1")
      .get("/"))
    .pause(7) // Note that Gatling has recorder real time pauses
    .exec(http("request_2")
      .get("//results?search_query=ms subbulakshmi"))
    .pause(2)
    .exec(http("request_3")
      .get("/results?search_query=chiranjeevi"))
    .pause(3)
    .exec(http("request_4")
      .get("/"))
    .pause(2)
    .exec(http("request_5")
      .get("/results?search_query=suchitra"))
    .pause(670 milliseconds)
    .exec(http("request_6")
      .get("/results?search_query=Mano"))
    .pause(629 milliseconds)
    .exec(http("request_7")
      .get("/results?search_query=SP Balu"))
    .pause(734 milliseconds)
    .exec(http("request_8")
      .get("/results?search_query=sp sailaja"))
    .pause(5)
    .exec(http("request_9")
      .get("/results?search_query=KV Mahadevan"))
    .pause(1)
    .exec(http("request_10") // Here's an example of a POST request
      .post("/computers")
      .formParam("""name""", """Beautiful Computer""") // Note the triple double quotes: used in Scala for protecting a whole chain of characters (no need for backslash)
      .formParam("""introduced""", """2012-05-30""")
      .formParam("""discontinued""", """""")
      .formParam("""company""", """37"""))

  setUp(scn.inject(atOnceUsers(50)).protocols(httpProtocol))
}
